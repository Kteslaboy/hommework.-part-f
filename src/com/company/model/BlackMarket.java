package com.company.model;

public class BlackMarket {
    public String name;
    public float course;

    public BlackMarket(String name, float course) {
        this.name = name;
        this.course = course;
    }

    public String getNameB() {
        return name;
    }

    public float getCourseB() {
        return course;
    }
}
