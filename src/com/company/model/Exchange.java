package com.company.model;

public class Exchange {
    public String nameEx;
    public float courseEx;
    public int limit;

    public Exchange(String name, float course, int limit) {
        this.nameEx = name;
        this.courseEx = course;
        this.limit = limit;
    }
    public int Lim(){
        return limit;
    }

    public String getNameEx() {
        return nameEx;
    }

    public float getCourseEx() {
        return courseEx;
    }
}
