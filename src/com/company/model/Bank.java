package com.company.model;

public class Bank {
    public String name;
    public float course;
    public int limit;

    public Bank(String name, float course, int limit) {
        this.name = name;
        this.course = course;
        this.limit = limit;
    }

    public int Lim() {
        return limit;
    }

    public String getName() {
        return name;
    }

    public float getCourse() {
        return course;
    }
}


