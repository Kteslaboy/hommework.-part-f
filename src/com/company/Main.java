package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Exchange;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bank[] banks = new Bank[3];
        banks[0] = new Bank("Pumb", 26.0f, 150000);
        banks[1] = new Bank("Privat", 26.33f, 150000);
        banks[2] = new Bank("Ukrsib", 26.6f, 150000);
        Exchange[] change = new Exchange[3];
        change[0] = new Exchange("change1", 27.0f, 5000);
        change[1] = new Exchange("change2", 26.6f, 5000);
        change[2] = new Exchange("change3", 27.3f, 5000);
        BlackMarket black = new BlackMarket("BlackMoney", 28.5f);
        Scanner in = new Scanner(System.in);
        System.out.println("Введите желаемую сумму для обмена: ");
        int sum = Integer.parseInt(in.nextLine());
        for (int i = 0; i < banks.length; i++) {
            if (sum < banks[i].Lim()) {
                System.out.println(String.format("Your money: %s %.2f:", banks[i].getName(), (sum / banks[i].getCourse()) * 0.95));
            }
            if (sum < change[i].Lim()) {
                System.out.println(String.format("Your money(exchage): %s %.2f:", change[i].getNameEx(), sum / change[i].getCourseEx()));
            }
        }
        System.out.println(String.format("Your money(BlackMarket): %s %.2f:", black.getNameB(), sum / black.getCourseB()));

    }
}

